(* Time-stamp: <modified the 15/04/2022 (at 10:34) by Erwan Jahier> *)
(*-----------------------------------------------------------------------
** This file may only be copied under the terms of the CeCill
** Public License
**-----------------------------------------------------------------------
**
** Author: erwan.jahier@univ-grenoble-alpes.fr
**
*)

let usage = " generate a plantuml file on stdout that can be processes by:
       java -jar plantuml.jar file.plantuml
 The plantuml.jar can be downloaded at https://plantuml.com/starting
 More information at https://plantuml.com/timing-diagram
"


(********************************************************************************)
let plantuml =
  try Unix.getenv "PLANTUML"
  with _ -> "plantuml"

(********************************************************************************)
open Data
let (subst_to_string : string * Data.v -> string) =
  fun (n,v) ->
  let rec v_to_str v =
    match v with
    | I i  -> (try string_of_int i with _ -> assert false)
    | F f  -> string_of_float f
    | B true -> "high"
    | B false -> "low"
    | E (e,_) -> e
    | S fl -> "{"^(String.concat ";"
                     (List.map (fun (fn,fv) -> fn^"="^(v_to_str fv)) fl))^"}"
    | A a -> 
      let str = ref "[" in
      let f i a = str := !str ^ (if i = 0 then "" else ",") ^ (v_to_str a) in
      Array.iteri f a;
      (!str^"]")
    | U -> "nil"
    | Str str -> str
  in
  Printf.sprintf "%s is %s" n (v_to_str v)

let pragmas =  ["quit";"outs"]
let (f: string -> unit) =
  fun rif_file ->
  let ic = open_in rif_file in
  let (inputs, outputs) = RifIO.read_interface ~debug:true  ic None in
  let put_one_var _is_input (var, t) =
    let kind = match t with
      | Bool ->   "binary"
      | Enum _ -> "concise"
      | _ -> "robust"
    in
    Printf.printf "%s \"%s:%s\" as %s\n" kind var (type_to_string t) var;
  in
  Printf.printf "@startuml\nTitle A visualisation of %s\n" Sys.argv.(1);
  List.iter (put_one_var true) inputs;
  List.iter (put_one_var false) outputs;
  let step = ref 1 in
  try
    while true do
      let sl = RifIO.read ~pragma:pragmas ic None (inputs@outputs) in
      Printf.printf "\n@%d\n%s\n" !step
        (String.concat " \n" (List.map subst_to_string sl));
      incr step
    done;
  with _ ->
    Printf.printf "@enduml\n%!"

let _ =
  if (Array.length Sys.argv)  <>  2
  then (
    Printf.printf "usage: %s <file.rif>\n%s\n%!" Sys.argv.(0) usage;
    exit 2)
  else f Sys.argv.(1)
