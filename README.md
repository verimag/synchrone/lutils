- [Installing `lutils` via opam](#org3d0c7d0)
- [Installing the git source](#org3d4fecf)
- [Tests](#org1c424bb)
- [Docker](#orgb88701a)

Tools and libs shared by other Verimag/synchronous tools (lustre, lutin, rdbg).

-   The lutils ocaml library contains various modules shared between tools. Those modules deal with:
    -   generate and parse RIF files
    -   generate dro files (to call luciole)
-   `gnuplot-rif` is a stand alone executable that vizualises RIF files using gnuplot.


<a id="org3d0c7d0"></a>

# Installing `lutils` via opam

```sh
$ opam repo add verimag-sync-repo "http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/opam-repository"
$ opam update
$ opam install lutils
```


<a id="org3d4fecf"></a>

# Installing the git source

```sh
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/lutils/lutils.git
cd lutils
opam install dune num
mv Makefile.version.hide Makefile.version
make configure
make
```


<a id="org1c424bb"></a>

# Tests

```sh
make test
```


<a id="orgb88701a"></a>

# Docker

nb: the content of this repo is also available via the `jahierwan/verimag-sync-tools` docker image
